package biz.c24.io.demo.gigaspaces;

import java.util.Date;

import org.openspaces.core.GigaSpace;
import org.openspaces.core.GigaSpaceConfigurer;
import org.openspaces.core.space.UrlSpaceConfigurer;

public class ProducerDemo {
	
	public static void main(String[] args) {
		
		// Connect to the Space
		UrlSpaceConfigurer configurer = new UrlSpaceConfigurer("jini://*/*/demoGrid");
		GigaSpace space = new GigaSpaceConfigurer(configurer).create();
		
		
		//-----------------------------------------
		// Demo 1 - Show versioning by extension
		// 
		// Writes an instance of a class and an instance of a class that extends it into the space
		// ----------------------------------------
		
		// Write 2 versions of Person into the space
		
		EmployeeV1 employeeV1 = new EmployeeV1();
		employeeV1.setId(1);
		employeeV1.setAge(32);
		employeeV1.setName("Bob Turner");
		employeeV1.setJobTitle("Trader");
		employeeV1.setAddress(new Address(25, "Bread St"));
		
		System.out.println("Writing EmployeeV1 - " + employeeV1.getName() + " to the space");
		space.write(employeeV1);
		
		EmployeeV2 employeeV2 = new EmployeeV2();
		employeeV2.setId(2);
		employeeV2.setAge(40);
		employeeV2.setName("Will Jones");
		employeeV2.setJobTitle("Accountant");
		employeeV2.setAddress(new Address(90, "Farley Rd"));
		employeeV2.setGrade("Junior");
		employeeV2.setAppraisals(new Appraisal[]{new Appraisal(9, new Date())});
		
	    System.out.println("Writing EmployeeV2 - " + employeeV2.getName() + " to the space");
		space.write(employeeV2);
		
		
		//-----------------------------------------
		// Demo 2 - Show versioning by interface
		// 
		// Writes multiple objects of disconnected types that implement the same interface into the space
		// Uses a wrapper class in the way as C24 iO SDOs so that clients can retrieve new types without being
		// Aware of them (GS queries are type-specific)
		// ----------------------------------------
		
		Wrapper wrapper1 = new Wrapper();
		wrapper1.setId(1);
		wrapper1.setPerson(employeeV1);
		System.out.println("Writing wrapped Person (EmployeeV1) - " + wrapper1.getPerson().getName() + " to the space");
		space.write(wrapper1);
		
		Wrapper wrapper2 = new Wrapper();
		wrapper2.setId(2);
		wrapper2.setPerson(employeeV2);
        System.out.println("Writing wrapped Person (EmployeeV2) - " + wrapper2.getPerson().getName() + " to the space");
		space.write(wrapper2);
		
		Customer customer = new Customer();
		customer.setId(1);
		customer.setName("Ben Williams");
		
		Wrapper wrapper3 = new Wrapper();
		wrapper3.setId(3);
		wrapper3.setPerson(customer);
	    System.out.println("Writing wrapped Person (Customer) - " + wrapper3.getPerson().getName() + " to the space");
		space.write(wrapper3);
	}

}
