package biz.c24.io.demo.gigaspaces;

import com.gigaspaces.annotation.pojo.SpaceId;
import com.gigaspaces.annotation.pojo.SpaceIndex;
import com.gigaspaces.annotation.pojo.SpaceIndexes;
import com.gigaspaces.metadata.index.SpaceIndexType;

public class Wrapper {
	
	private Integer id;
	private Person person;
	
	@SpaceId
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

    @SpaceIndexes({@SpaceIndex(path="name" , type = SpaceIndexType.BASIC)})
	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

}
