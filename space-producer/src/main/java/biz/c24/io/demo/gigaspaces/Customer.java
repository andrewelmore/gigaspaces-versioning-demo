package biz.c24.io.demo.gigaspaces;

import java.io.Serializable;

public class Customer implements Person, Serializable {

	private String name;
	private Integer id;
	
	@Override
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
}
