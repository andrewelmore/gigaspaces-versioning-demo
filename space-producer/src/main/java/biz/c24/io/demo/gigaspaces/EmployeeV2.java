package biz.c24.io.demo.gigaspaces;

public class EmployeeV2 extends EmployeeV1 {
    private String grade;
    private Appraisal[] appraisals;

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

    public Appraisal[] getAppraisals() {
        return appraisals;
    }

    public void setAppraisals(Appraisal[] appraisals) {
        this.appraisals = appraisals;
    }
}
