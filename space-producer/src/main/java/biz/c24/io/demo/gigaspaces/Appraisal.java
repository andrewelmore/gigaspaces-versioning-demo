package biz.c24.io.demo.gigaspaces;

import java.io.Serializable;
import java.util.Date;

public class Appraisal implements Serializable {
    private int overallScore;
    private Date date;
    
    public Appraisal() {
        
    }
    
    public Appraisal(int overallScore, Date date) {
        super();
        this.overallScore = overallScore;
        this.date = date;
    }
  
    public int getOverallScore() {
        return overallScore;
    }
    public void setOverallScore(int overallScore) {
        this.overallScore = overallScore;
    }
    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }

}
