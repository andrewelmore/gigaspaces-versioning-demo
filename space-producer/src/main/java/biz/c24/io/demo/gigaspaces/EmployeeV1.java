package biz.c24.io.demo.gigaspaces;

import java.io.Serializable;

import com.gigaspaces.annotation.pojo.SpaceId;


public class EmployeeV1 implements Person, Serializable {
    private Integer age;
    private String name;
    private String jobTitle;
    private Address address;
    
    private Integer id;
    

    
    
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@SpaceId
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
    public String getJobTitle() {
        return jobTitle;
    }
    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }
    public Address getAddress() {
        return address;
    }
    public void setAddress(Address address) {
        this.address = address;
    }
}

