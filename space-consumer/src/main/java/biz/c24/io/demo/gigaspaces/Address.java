package biz.c24.io.demo.gigaspaces;

import java.io.Serializable;

public class Address implements Serializable {
    

    private int number;
    private String street;
    
    public Address() {
        
    }
    
    public Address(int number, String street) {
        super();
        this.number = number;
        this.street = street;
    }
    
    public int getNumber() {
        return number;
    }
    public void setNumber(int number) {
        this.number = number;
    }
    public String getStreet() {
        return street;
    }
    public void setStreet(String street) {
        this.street = street;
    }

}
