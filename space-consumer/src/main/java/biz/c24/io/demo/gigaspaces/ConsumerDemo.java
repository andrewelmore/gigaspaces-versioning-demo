package biz.c24.io.demo.gigaspaces;

import org.openspaces.core.GigaSpace;
import org.openspaces.core.GigaSpaceConfigurer;
import org.openspaces.core.space.UrlSpaceConfigurer;

public class ConsumerDemo {
	
	public static void main(String[] args) {

		// Connect to the Space
		UrlSpaceConfigurer configurer = new UrlSpaceConfigurer("jini://*/*/demoGrid");
		GigaSpace space = new GigaSpaceConfigurer(configurer).create();
		
		
		//-----------------------------------------
		// Demo 1 - Show versioning by extension
		// 
		// In the space there are instances of EmployeeV1 and EmployeeV2 (which extends EmployeeV1).
		// We only know about EmployeeV1 (EmployeeV2 is not in our classpath)
		// Show that a query for EmployeeV1 will also retrieve and successfully process the EmployeeV2 instance
		// ----------------------------------------
		
		System.out.println("Processing employees:");
		EmployeeV1[] employees = space.readMultiple(new EmployeeV1());
		
		for(EmployeeV1 person : employees) {
			System.out.println(person.getName() + " - " + person.getJobTitle() + " (" + person.getAge() + ") @ " + person.getAddress().getNumber() + " " + person.getAddress().getStreet());
		}
		
		System.out.println();
	
		
		//-----------------------------------------
		// Demo 2 - Show versioning by interface
		// 
		// Shows that we can read and process any object in the space that implements interface Person
		// even though we know nothing about the underlying class.
		// As GS-queries must use concrete types, we store the objects inside a wrapper of a known type
		// ----------------------------------------
	
		System.out.println("Processing people:");
		Wrapper[] people = space.readMultiple(new Wrapper());
		
		for(Wrapper person : people) {
			System.out.println(person.getPerson().getName());
		}
		
	}

}
