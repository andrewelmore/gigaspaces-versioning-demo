package biz.c24.io.demo.gigaspaces;

import com.gigaspaces.annotation.pojo.SpaceId;

public class Wrapper {
	
	private Integer id;
	private Person person;
	
	@SpaceId
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

}
