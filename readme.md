Versioning in Gigaspaces
========================

A simple demo to show how producers can insert new versions and types of object into 
a Gigaspaces space and still have existing consumers process them.

The Producer and Consumer are in 2 separate completely disconnected projects so we can 
ensure that at compile and runtime the Consumer project does not have visibility of the Producer's classes.

Instructions to run locally
---------------------------

The projects are set-up to run on XAP 9.7.0. It is important that the version of Gigaspaces
used by the projects is the same as the version you are running, otherwise the JINI lookup
of the space will fail. To use a different version you need to update the gsVersion property in both the space-consumer and space-producer POMs.

For example to use XAP 9.6.2 change:

        <gsVersion>9.7.0-10496-RELEASE</gsVersion>

to

        <gsVersion>9.6.2-9900-RELEASE</gsVersion>

The jars are installed to your local Maven repository when you run the installmavenrep.sh/.bat from your Gigaspaces installation directory. You can look in your local Maven repository to see which versions of the gs-openspaces jars have been installed.


1. Start up the agent:

	NIC_ADDR=127.0.0.1 gs-agent.sh gsa.global.lus 0 gsa.lus 1

2. Create the space:

	NIC_ADDR=127.0.0.1 gs.sh deploy-space -cluster total_members=1,1 demoGrid

3. Run the ProducerDemo class to populate the space (use the gs-ui to see the objects in the space)

	cd space-producer
	
	mvn compile exec:java
	
	cd ..

4. Run the ConsumerDemo to show it Consumer reading all the objects from the space, despite the fact that it only has a subset of the class definitions in its classpath

	cd space-consumer
	
	mvn compile exec:java
	
	cd ..



